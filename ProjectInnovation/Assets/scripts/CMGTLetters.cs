﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMGTLetters : MonoBehaviour
{

    public enum Letter
    {
        C,
        M,
        G,
        T
    }
    [SerializeField]private Letter _Letter;

    public Letter GetLetter()
    {
        return this._Letter;
    }
}
