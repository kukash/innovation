﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OculusGrab : MonoBehaviour
{
    public enum HandSide
    {
        LEFT,
        RIGHT
    }
    public HandSide handSide;
    private GameObject _collidingObject;
    private GameObject _objectInHand;
    [SerializeField] private OVRPlayerGrabber grabber;
       
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject!=null && other.tag=="Grabable")
        {
            
         //   Debug.Log("Collision enter");
            Collision(other.gameObject);
        }
        if (other.gameObject != null && other.tag == "Gun")
        {
            Collision(other.gameObject);
            if (other.gameObject.GetComponent<shoot>())
            {
                shoot shootScript = other.gameObject.GetComponent<shoot>();
                shootScript.enterHand();
                other.gameObject.transform.rotation = Quaternion.identity;
            }
        }
        if (other.gameObject != null && other.tag == "Letter" || other.tag=="Pen")
        {
            Collision(other.gameObject);
        }
    }
    //remove object on collision exit
    private void OnTriggerExit(Collider other)
    {
        _collidingObject = null;
        grabber.getCollisionExit();
    }
    public void Collision(GameObject colliderObject)
    {
        grabber.getCollision(colliderObject, this.transform.gameObject);
    }
}
