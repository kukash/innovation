﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draw : MonoBehaviour
{
    private bool _leftHand = false;

    private bool _canDraw = false;
    [SerializeField]
    private Transform _trackingSpace;
    private LineRenderer LR;
    private void Start()
    {
        LR = GetComponent<LineRenderer>();
    }
    // Update is called once per frame
    void Update()
    {
        if (transform.parent != null)
        {
            //   Debug.Log(this.transform.parent.gameObject.layer);

        }
        if (_canDraw)
        {
            if (LR) LR.enabled = true;
            else Debug.Log("no lineRenderer");

            //draw ray
            Matrix4x4 localToWorld = _trackingSpace.localToWorldMatrix;
            Quaternion orientation = OVRInput.GetLocalControllerRotation(GetController());

            Vector3 localStartPos = OVRInput.GetLocalControllerPosition(GetController());
            Vector3 localEndPos = localStartPos + ((orientation * Vector3.forward) * 500.0f);

            Vector3 worldStartPos = localToWorld.MultiplyPoint(localStartPos);
            Vector3 worldEndPos = localToWorld.MultiplyPoint(localEndPos);
            LR.SetPosition(0, worldStartPos);
            LR.SetPosition(1, worldEndPos);

            if (Input.GetAxis("Oculus_CrossPlatform_PrimaryIndexTrigger") > 0.2f &&_leftHand
               || Input.GetAxis("Oculus_CrossPlatform_SecondaryIndexTrigger") > 0.2f&&!_leftHand
               )
            {
                //cast ray check for interaction with Teleport Layer 
                Ray ray = new Ray(worldStartPos, worldEndPos - worldStartPos);
                RaycastHit hit;
                LayerMask mask = LayerMask.GetMask("Draw");
                if (Physics.Raycast(ray, out hit, 500f, mask))
                {
                    //cast the ray
                    if (hit.transform.gameObject.tag == "DrawAble")
                    {
                        Material material = hit.transform.gameObject.GetComponent<Renderer>().material;
                        if (material)
                        {
                            material.SetColor("_Colour", Color.black);
                        }
                        else
                        {
                            Debug.Log("mat is null");
                        }
                    }
                }

            }
        }
        else
        {
            LR.enabled = false;
            if (transform.parent)
            {

                if (transform.parent.gameObject.layer == 10)
                {
                    _canDraw = true;
                    if (transform.parent.gameObject.GetComponent<OculusGrab>())
                    {
                        if (transform.parent.gameObject.GetComponent<OculusGrab>().handSide == OculusGrab.HandSide.LEFT)
                        {
                            _leftHand = true;
                        }
                        else if (transform.parent.gameObject.GetComponent<OculusGrab>().handSide == OculusGrab.HandSide.RIGHT)
                        {
                            _leftHand = false;
                        }
                    }
                }

            }
        }

    }

    public void EnterHand()
    {
    }
    public void LeaveHand()
    {
        Debug.Log("left Hand");
        _canDraw = false;

    }
    private OVRInput.Controller GetController()
    {
        if (_leftHand) return OVRInput.Controller.LTrackedRemote;
        else return OVRInput.Controller.RTrackedRemote;
    }
}
