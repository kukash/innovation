﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class TeleportManager : MonoBehaviour
{
    public static event Action<Transform> DoTeleport;
    [SerializeField] Transform reticleTransform;

     void OnEnable()
    {
        
    }
     void OnDisable()
    {
        
    }
    public void Teleport(Vector3 position)
    {

        if (DoTeleport != null)
        {

            DoTeleport(reticleTransform);
        }
        else
        {
           
        }
    }
}
