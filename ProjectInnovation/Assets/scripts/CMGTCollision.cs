﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMGTCollision : MonoBehaviour
{
    private CMGTLetters thisLetter;
    private Renderer _renderer;
    private void Start()
    {
        thisLetter = GetComponent<CMGTLetters>();
        _renderer = GetComponent<Renderer>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Letter")
        {
            CMGTLetters otherLetter = collision.gameObject.GetComponent<CMGTLetters>();
            if (otherLetter.GetLetter() == thisLetter.GetLetter())
            {
                Debug.Log("got correct letter");
                _renderer.material.SetInt("_isPlaced", 1);
                Destroy(otherLetter.transform.gameObject);
            }
        }
    }
}
