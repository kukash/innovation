﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OVRPlayerGrabber : MonoBehaviour
{
    private GameObject _collidingObject;
    private GameObject _objectInHand;
    private GameObject _hand;
    private Vector3 handPosLeft;
    private Quaternion orientationLeft;
    private Quaternion orientationRight;
    private Quaternion destOrientation;

    private Vector3 handPosRight;
    private Vector3 destPos;

    [SerializeField] private Transform gripLeft;
    [SerializeField] private Transform gripRight;
    [SerializeField] private Transform parentTrans;

    private void Update()
    {
        if (_collidingObject != null)
        {
            if (Input.GetAxis("Oculus_CrossPlatform_SecondaryHandTrigger") > 0.2f) GrabRight();
            else if (Input.GetAxis("Oculus_CrossPlatform_PrimaryHandTrigger") > 0.2f) GrabLeft();

            else if (Input.GetAxis("Oculus_CrossPlatform_SecondaryHandTrigger") < 0.2f) ReleaseObject();
        }

    }

    public void getCollision(GameObject colliderObject, GameObject Hand)
    {
        _collidingObject = colliderObject;
        _hand = Hand;
        if (_collidingObject != null)
        {
            if (_collidingObject.transform.tag == "Grabable" || _collidingObject.transform.tag == "Gun" 
                || _collidingObject.transform.tag == "Pen"|| _collidingObject.transform.tag == "Letter")
            {
                moveToHand moveHand = _collidingObject.GetComponent<moveToHand>();
                moveHand.reachedHand();
                //see if object is already being grabbed
              //  Debug.Log("grabable in range");
                if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger) > 0.2f && Hand.GetComponent<OculusGrab>().handSide == OculusGrab.HandSide.LEFT
                    || OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger) > 0.2f && Hand.GetComponent<OculusGrab>().handSide == OculusGrab.HandSide.RIGHT)
                {
                    Debug.Log("grabbing");
                    GrabObject();
                }
            }
        }
    }
    public void getCollisionExit()
    {
        _collidingObject = null;
        _hand = null;
    }
    private void GrabLeft()
    {
        if (_hand != null && _hand.GetComponent<OculusGrab>().handSide == OculusGrab.HandSide.LEFT)
        {
            handPosLeft = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTrackedRemote);
            destPos = parentTrans.transform.TransformPoint(gripLeft.localPosition + handPosLeft);

            orientationLeft = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTrackedRemote);
            destOrientation = parentTrans.transform.rotation * orientationLeft;

            GrabObject();
        }
    }
    private void GrabRight()
    {
        if (_hand != null && _hand.GetComponent<OculusGrab>().handSide == OculusGrab.HandSide.RIGHT)
        {

            handPosRight = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTrackedRemote);
            destPos = parentTrans.transform.TransformPoint(gripRight.localPosition + handPosRight);

            orientationRight = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote);
            destOrientation = parentTrans.transform.rotation * orientationRight;

            GrabObject();
        }
    }
    private void GrabObject()
    {
        if (_collidingObject != null)
        {

            // Vector3 handPos = OVRInput.GetLocalControllerPosition(_hand);
            //Quaternion handRot = OVRInput.GetLocalControllerRotation(m_controller);
            //  destPos = _hand.transform.TransformPoint(handOffset + handPosLeft);
            //Quaternion destRot = m_parentTransform.rotation * handRot * m_anchorOffsetRotation;
            //GetComponent<Rigidbody>().MoveRotation(destRot);


            //Debug.Log("Grabbing");
            _objectInHand = _collidingObject;
            _objectInHand.GetComponent<Rigidbody>().MovePosition(destPos);
            _objectInHand.GetComponent<Rigidbody>().MoveRotation(destOrientation);

            _objectInHand.transform.SetParent(_hand.transform);
            _objectInHand.GetComponent<Rigidbody>().isKinematic = true;
        }
    }
    private void ReleaseObject()
    {
        if (_objectInHand != null)
        {
            if (_collidingObject.transform.tag == "Grabable" || _collidingObject.transform.tag =="Gun" 
                || _collidingObject.transform.tag =="Pen" || _collidingObject.transform.tag =="Letter")
            {
                moveToHand moveHand = _collidingObject.GetComponent<moveToHand>();
                moveHand.dropped();
            }
            _objectInHand.GetComponent<Rigidbody>().isKinematic = false;
            _objectInHand.transform.parent = null;
            _objectInHand = null;
            _collidingObject = null;
        }
    }

    private void ThrowObject()
    {
    }
}
