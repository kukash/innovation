﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class drawAble : MonoBehaviour
{

    [SerializeField] private GameObject brushPrefab;
    [SerializeField] private float brushSize;
    public Transform _trackingSpace;
    public RenderTexture rText;
     void Update()
    {
        if (OVRInput.Get(OVRInput.RawButton.A))
        {
            Vector3 localStartPos = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTrackedRemote);
            Matrix4x4 localToWorld = _trackingSpace.localToWorldMatrix;
            Vector3 worldStartPos = localToWorld.MultiplyPoint(localStartPos);
            Quaternion orientation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.RTrackedRemote);
            Vector3 localEndPos = localStartPos + ((orientation * Vector3.forward) * 500.0f);
            Vector3 worldEndPos = localToWorld.MultiplyPoint(localEndPos);
            Ray ray = new Ray(worldStartPos + (worldEndPos - worldStartPos).normalized, worldEndPos-worldStartPos);
            RaycastHit hit;
            LayerMask mask = LayerMask.GetMask("Draw");
            LineRenderer LR = GetComponent<LineRenderer>();
            LR.SetPosition(0, ray.origin);
            LR.SetPosition(1, worldEndPos);
            if (Physics.Raycast(ray, out hit,500f))
            {
               
                //Debug.Log("casting ray");
                if (hit.transform.gameObject.tag=="DrawAble")
                {
                    GameObject brush = Instantiate(brushPrefab, hit.point + Vector3.forward * 0.1f, Quaternion.identity, transform);
                    brush.transform.localScale = Vector3.one * brushSize;
                    brush.transform.rotation = Quaternion.Euler(90,0,0);
                    CoSafe();

                }
            }
        }
    }
    private IEnumerator CoSafe()
    {
        Debug.Log("saving" + Application.dataPath + "/savedImage");

        yield return new WaitForEndOfFrame();
        RenderTexture.active = rText;

        var text2D = new Texture2D(rText.width, rText.height);
        text2D.ReadPixels(new Rect(0, 0, rText.width, rText.height), 0, 0);
        text2D.Apply();

        var data = text2D.EncodeToJPG();
        File.WriteAllBytes(Application.dataPath + "/savedImage", data);
        Debug.Log("saving" + Application.dataPath + "/savedImage");
    }

}
