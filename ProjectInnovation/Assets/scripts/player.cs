﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    Vector3 playerPos;

    [SerializeField] private float playerHeight = 1.8f;
    private void OnEnable()
    {
        TeleportManager.DoTeleport += MoveTo;
    }
    private void OnDisable()
    {
        TeleportManager.DoTeleport -= MoveTo;
    }



    private void MoveTo(Transform destinationTransform)
    {
        playerPos = destinationTransform.position;
        playerPos.y= playerHeight;
        this.transform.position = playerPos;
    }
}
