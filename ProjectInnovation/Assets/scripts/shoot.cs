﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoot : MonoBehaviour
{
    private bool _doShoot;
    private Rigidbody _rb;

    [SerializeField] private GameObject bullet;
    [SerializeField] private float shootDelay;
    [SerializeField] private float speed;
    [SerializeField] private Transform muzzleEnd;

    private GameObject bulletParent;
    private float _t;
    
    // Start is called before the first frame update
    void Start()
    {
        _rb = this.GetComponent<Rigidbody>();
        bulletParent = new GameObject();
        _t = shootDelay;
    }

    // Update is called once per frame
    void Update()
    {
        _t += Time.deltaTime;
        if (this.transform.parent != null)
        {
         //   Debug.Log(this.transform.parent.gameObject.layer);
            if (this.transform.parent.gameObject.layer == 10)
            {
            //    Debug.Log("Gun is in hand");
                _doShoot = true;
            }
        }
        else _doShoot = false;
        if (_doShoot)
        {
            if (Input.GetAxis("Oculus_CrossPlatform_PrimaryIndexTrigger") > 0.2f 
                || Input.GetAxis("Oculus_CrossPlatform_SecondaryIndexTrigger") > 0.2f
                && _t>=shootDelay
                )
            {
                _t = 0;
                Matrix4x4 localToWorld = transform.localToWorldMatrix;
                Vector3 spawnPos = localToWorld.MultiplyPoint(muzzleEnd.position);
                GameObject newBullet=Instantiate(bullet, muzzleEnd.position,Quaternion.identity);
                Rigidbody bulletRB = bullet.GetComponent<Rigidbody>();
                if (bulletRB != null)
                {
                    //  bulletRB.AddForce(new Vector3(0,speed,0),ForceMode.e);
                   debugBullet bulletScript=newBullet.GetComponent<debugBullet>();
                    bulletScript.shootBullet(this.transform.forward, speed);
                  //  Debug.Log("Adding force" + Vector3.forward * speed);

                }
                else
                {
                    Debug.LogWarning("CANNOT ACCES BULLET RB");
                }
                //bulletRB.AddForce(new Vector3(10, 0, 0));
               //   bulletRB.AddForce(Vector3.forward * speed);
                bulletRB.velocity = Vector3.forward * speed;
                    //new Vector3(100, 0, 0);
            }
        
        }
    }
    public void enterHand()
    {
        _doShoot = true;
    }
    public void leaveHand()
    {
        _doShoot = false;
    }
}
