﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveToHand : MonoBehaviour
{
    private bool _doMove;
    private Vector3 _handPos;
    private Vector3 _thisPos;
    private float _speed;
    private bool _reachedHand;
    private GameObject _Hand;
    private void Start()
    {
        _reachedHand = false;
    }
    private void Update()
    {
        if (_doMove && !_reachedHand)
        {
            Vector3 deltaPos = _handPos - _thisPos;
            Vector3 dirVec = deltaPos.normalized;
            float dist = deltaPos.magnitude;
            if (dist > 0.2f)
            {
                _thisPos += dirVec * Mathf.Clamp((_speed / dist) * Time.deltaTime, 0.05f, 1);
                transform.position = _thisPos;
                Debug.Log("moving");
            }
            else
            {
                OculusGrab grabber = _Hand.GetComponent<OculusGrab>();
                grabber.Collision(gameObject);
                Debug.Log("reached hand");

            }
        }
    }
    public void Move(Vector3 handPos, float grabSpeed, GameObject hand)
    {
        _doMove = true;
        _Hand = hand;
        _thisPos = transform.position;
        _handPos = handPos;
        _speed = grabSpeed;
    }
    public void stopMove()
    {
        _doMove = false;
    }
    public void reachedHand()
    {
        _reachedHand = true;
    }
    public void dropped()
    {
        _reachedHand = false;
    }
    private void Reset()
    {
        _doMove = false;
        _reachedHand = false;
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.isKinematic = false;
        // _Hand = null;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer != 10)
        {
            Debug.Log(collision.gameObject.layer);
            Debug.Log("colliding");

            Reset();

            if (transform.tag == "Gun")
            {
                if (gameObject.GetComponent<shoot>())
                {
                    shoot shootScript = gameObject.GetComponent<shoot>();
                    shootScript.leaveHand();
                }
            }
            else if (transform.tag == "Pen")
            {
                if (gameObject.GetComponent<Draw>())
                {
                    Draw drawScript = gameObject.GetComponent<Draw>();
                    drawScript.LeaveHand();
                }
            }


        }
        else
        {
            Debug.Log("did not get object");
        }
    }
}
