﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugBullet : MonoBehaviour
{
    private Vector3 dir;
    private float speed;
    private Rigidbody _rb;
    private void Start()
    {
        float newX= this.transform.rotation.x + 90;
        float newZ = this.transform.rotation.z + 90;
        transform.rotation = Quaternion.Euler(newX, transform.rotation.y, transform.rotation.z);
    }
    void Update()
    {
        _rb.AddForce(dir * speed, ForceMode.Impulse);
    }
    public void shootBullet (Vector3 startDir, float bulletSpeed)
    {
        dir= startDir;
        speed = bulletSpeed;
        _rb = GetComponent<Rigidbody>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("bullet hit");
        if (collision.transform.tag == "Target")
        {
            collision.gameObject.GetComponent<targetHit>().gotHit();
        }
        Destroy(this.transform.gameObject);
    }
}
