namespace Oculus.Platform.Samples.VrHoops
{
    using UnityEngine;
    using UnityEngine.UI;

    // Helper class to attach to the main camera that raycasts where the
    // user is looking to select/deselect Buttons.
    public class VREyeRaycaster : MonoBehaviour
    {
        [SerializeField] private LineRenderer _lineRenderer = null;
        [SerializeField] private Transform _trackingSpace;
        [SerializeField] private bool _useNodeTeleport;
        public GameObject LeftHand;
        public GameObject RightHand;
        public reticle _reticle;
        public TeleportManager teleportManager;
        public bool ShowLineRendere;
        private bool previousState = false;
        private bool currentState = false;
        private bool hitPrevious = false;
        private Vector3 oldDestinationPos = Vector3.zero;
        private Material previousMat;
        private moveToHand currentObject;
        private bool movedObject;
        private bool grabingObject = false;
        private GameObject currentHand;
        [SerializeField] private ParticleSystem _particles;

        private void Start()
        {
        }
        private bool getControllerInput()
        {
            OVRInput.Controller controller = OVRInput.GetConnectedControllers() & (OVRInput.Controller.LTrackedRemote | OVRInput.Controller.RTrackedRemote);

            if (OVRInput.Get(OVRInput.Button.PrimaryThumbstickUp)) return true;
            if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickUp)) return true;
            return false;
        }
        private OVRInput.Controller Controller()
        {
            OVRInput.Controller controller = OVRInput.GetConnectedControllers();
            if (OVRInput.Get(OVRInput.Button.PrimaryThumbstickUp))
            {
                return OVRInput.Controller.LTrackedRemote;
            }
            if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickUp))
            {
                return OVRInput.Controller.RTrackedRemote;
            }

            return OVRInput.GetActiveController();
        }
        private bool getGrabInput()
        {

            if (OVRInput.Get(OVRInput.Button.PrimaryThumbstickDown)) return true;
            if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickDown)) return true;
            //   if (OVRInput.Get(OVRInput.Button.One)) return true;
            //   if (OVRInput.Get(OVRInput.Button.Three)) return true;

            //   if (Input.GetAxis("Oculus_CrossPlatform_SecondaryHandTrigger") > 0.2f) return true;
            //   if (Input.GetAxis("Oculus_CrossPlatform_PrimaryHandTrigger") > 0.2f) return true;

            // if (OVRInput.Get(OVRInput.RawButton.LIndexTrigger)) return true;
            //if (OVRInput.Get(OVRInput.RawButton.RIndexTrigger)) return true;
            return false;
        }
        private OVRInput.Controller getGrabController()
        {



            if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickDown)) return OVRInput.Controller.RTrackedRemote;
            if (OVRInput.Get(OVRInput.Button.PrimaryThumbstickDown)) return OVRInput.Controller.LTrackedRemote;

            //uses hand trigger
            //if (Input.GetAxis("Oculus_CrossPlatform_PrimaryHandTrigger") > 0.2f)
            //{
            //    return OVRInput.Controller.LTrackedRemote;
            //}
            //if (Input.GetAxis("Oculus_CrossPlatform_SecondaryHandTrigger") > 0.2f)
            //{
            //    return OVRInput.Controller.RTrackedRemote;
            //}
            return OVRInput.GetActiveController();
        }
        private bool rightGrabInput()
        {
            if (Input.GetAxis("Oculus_CrossPlatform_SecondaryHandTrigger") > 0.2f) return true;
            return false;
        }
        private bool leftGrabInput()
        {
            if (Input.GetAxis("Oculus_CrossPlatform_PrimaryHandTrigger") > 0.2f) return true;
            return false;
        }
        private void Teleportation()
        {
            //init temp vars for update 
            Material currentMat = null;
            bool didHit = false;
            RaycastHit hit;
            Vector3 worldStartPos = Vector3.zero;
            Vector3 worldEndPos = Vector3.zero;

            //check if you got input
            if (getControllerInput() && _trackingSpace != null)
            {
                currentState = true;

                //get controller position for laser start 
                Matrix4x4 localToWorld = _trackingSpace.localToWorldMatrix;
                Quaternion orientation = OVRInput.GetLocalControllerRotation(Controller());

                Vector3 localStartPos = OVRInput.GetLocalControllerPosition(Controller());
                Vector3 localEndPos = localStartPos + ((orientation * Vector3.forward) * 500.0f);

                worldStartPos = localToWorld.MultiplyPoint(localStartPos);
                worldEndPos = localToWorld.MultiplyPoint(localEndPos);
                worldStartPos = worldStartPos + (worldEndPos - worldStartPos).normalized * 0.2f;
                //cast ray check for interaction with Teleport Layer 
                Ray ray = new Ray(worldStartPos, (worldEndPos - worldStartPos));
                LayerMask mask = LayerMask.GetMask("Teleport");
                _particles.transform.position = worldStartPos;
                //_particles.transform.rotation = Quaternion.Euler(Vector3.RotateTowards(worldEndPos, worldEndPos));
                //Quaternion.FromToRotation(worldStartPos, worldEndPos);
                _particles.transform.rotation = Quaternion.LookRotation(worldEndPos - worldStartPos);
                
                //_particles.transform.rotation = Quaternion.Euler(_particles.transform.rotation.x, _particles.transform.rotation.y, _particles.transform.rotation.z + 90);

                _particles.Play();
                if (Physics.Raycast(ray, out hit))
                {
                    if (_useNodeTeleport)
                    {
                        if (hit.transform.tag == "TPNode")
                        {
                            didHit = true;
                        }
                    }
                    else didHit = true;
                    //if (hit.transform.tag == "TPNode")
                    //{
                    //    currentMat = hit.transform.gameObject.GetComponent<Renderer>().material;
                    //    currentMat.SetInt("_Outline", 1);
                    //}
                    worldEndPos = hit.point;
                    //mask = LayerMask.GetMask("Grabable");
                    //if (Physics.Raycast(ray, out hit, 500f, mask))
                    //{
                    //    if (hit.transform.tag == "Grabable" || hit.transform.tag == "Gun")
                    //    {
                    //        currentMat = hit.transform.gameObject.GetComponent<Renderer>().material;
                    //        Debug.Log("grabbable");
                    //        if (currentMat != null) currentMat.SetInt("_Outline", 1);
                    //        else Debug.Log("null");
                    //    }
                    //    worldEndPos = hit.point;
                    //}
                }

            }
            else currentState = false;

            //cast line renderer
            if (getControllerInput() && _lineRenderer != null)
            {
                _lineRenderer.SetPosition(0, worldStartPos);
                _lineRenderer.SetPosition(1, worldEndPos);
            }
            //teleport if button was released and you hit something with ray
            if (currentState == false && previousState == true && hitPrevious)
            {

                if (teleportManager != null)
                {
                    _reticle.setPosition(oldDestinationPos);
                    teleportManager.Teleport(worldEndPos);
                }
            }

            oldDestinationPos = worldEndPos;
            hitPrevious = didHit;
            previousState = currentState;
            if (previousMat != null && !didHit) previousMat.SetInt("_Outline", 0);
            previousMat = currentMat;
        }

        private void DistanceGrab()
        {
            RaycastHit hit;
            if (getGrabInput() && _trackingSpace != null)
            {
                //get controller position for ray start 
                Matrix4x4 localToWorld = _trackingSpace.localToWorldMatrix;
                Quaternion orientation = OVRInput.GetLocalControllerRotation(getGrabController());

                Vector3 localStartPos = OVRInput.GetLocalControllerPosition(getGrabController());
                Vector3 localEndPos = localStartPos + ((orientation * Vector3.forward) * 500.0f);

                Vector3 worldStartPos = localToWorld.MultiplyPoint(localStartPos);
                Vector3 worldEndPos = localToWorld.MultiplyPoint(localEndPos);

                //cast ray check for interaction with Teleport Layer 
                Ray ray = new Ray(worldStartPos, worldEndPos - worldStartPos);
                LayerMask mask = LayerMask.GetMask("Grabable");
                if (Physics.Raycast(ray, out hit, 500f, mask))
                {
                    if (hit.transform.tag == "Grabable" || hit.transform.tag == "Gun" || hit.transform.tag == "Pen" || hit.transform.tag == "Letter")
                    {
                        //if (hit.transform.gameObject.GetComponent<Renderer>())
                        //{
                        //    Material newMat = hit.transform.gameObject.GetComponent<Renderer>().material;
                        //    if (newMat != null) newMat.SetInt("_Highlight", 1);
                        //}

                        if ((getGrabController() == OVRInput.Controller.RTrackedRemote) && (rightGrabInput())
                            || (getGrabController() == OVRInput.Controller.LTrackedRemote) && (leftGrabInput()))
                        {
                            if ((getGrabController() == OVRInput.Controller.RTrackedRemote)) currentHand = RightHand;
                            else if (getGrabController() == OVRInput.Controller.LTrackedRemote) currentHand = LeftHand;
                            currentObject = hit.transform.GetComponent<moveToHand>();
                            if (currentObject != null) currentObject.Move(worldStartPos, 1, currentHand);
                            movedObject = true;
                        }
                    }
                }

                _lineRenderer.SetPosition(0, worldStartPos);
                _lineRenderer.SetPosition(1, worldEndPos);


            }
        }
        void Update()
        {
            _particles.Stop();

            //check if linerender exists
            if (_lineRenderer != null)
            {
                _lineRenderer.enabled = getControllerInput() || getGrabInput() && ShowLineRendere;
                // _particles.Play();
            }
            else
            {
            }
            movedObject = false;
            Teleportation();
            if (!grabingObject)
            {
                DistanceGrab();
            }
            if ((OVRInput.GetUp(OVRInput.RawButton.LHandTrigger)) || OVRInput.GetUp(OVRInput.RawButton.RHandTrigger))
            {
                if (currentObject != null) currentObject.stopMove();
            }
        }

    }

}
